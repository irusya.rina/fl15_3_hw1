const { filePath } = require('../config');
const fs = require('fs').promises;
const isFilenamePresent = require('../validators/isFilenamePresent');

module.exports = async (req, res) => {
    const filename = req.params.filename;
    const path = `${filePath}/${filename}`;

    await isFilenamePresent(path, filename);
    await fs.unlink(path);
    
    res.json({ message: 'File removed successfully' });
}