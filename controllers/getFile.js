const { filePath } = require('../config');
const fs = require('fs').promises;
const isFilenamePresent = require('../validators/isFilenamePresent');

module.exports = async (req, res) => {
    const filename = req.params.filename;
    const path = `${filePath}/${filename}`;

    await isFilenamePresent(path, filename);

    const findedFileData = await fs.readFile(path, 'utf8');
    const { birthtime } = await fs.stat(path);
    const extension = filename.split('.').pop();

    res.json({
        message: 'Success',
        filename: filename,
        content: findedFileData,
        extension: extension,
        uploadedDate: birthtime
    });
}