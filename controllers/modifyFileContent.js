const { filePath } = require('../config');
const fs = require('fs').promises;
const isFilenamePresent = require('../validators/isFilenamePresent');

module.exports = async (req, res) => {
    const filename = req.params.filename;
    const path = `${filePath}/${filename}`;
    const { content } = req.body;

    await isFilenamePresent(path, filename);
    await fs.writeFile(path, content, 'utf8');

    const { mtime } = await fs.stat(path);
    
    res.json({
        message: 'Success',
        filename: filename,
        content: content,
        updatedDate: mtime
    });
}