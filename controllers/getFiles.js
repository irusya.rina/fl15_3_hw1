const { filePath } = require('../config');
const fs = require('fs').promises;
const createError = require('http-errors');

module.exports = async (req, res) => {
    try {
        const filesList = await fs.readdir(filePath);

        res.json({
            message: "Success",
            files: filesList
        });
    } catch (err) {
        throw createError(400, 'Client error');
    }
}