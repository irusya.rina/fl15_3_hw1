const fs = require('fs').promises;
const { filePath } = require('../config');
const isValidFileExtension = require('../validators/isValidFileExtension');
const isContentExist = require('../validators/isContentExist');
const isFilenameExist = require('../validators/isFilenameExist');

module.exports = async (req, res) => {
    const { filename, content } = req.body;
    isFilenameExist(filename);
    isContentExist(content);

    const extension = filename.split('.').pop();
    isValidFileExtension(extension);

    try {
        await fs.mkdir(filePath);
    } catch { }

    const path = `${filePath}/${filename}`;

    await fs.writeFile(path, content, 'utf8');
    res.json({ message: 'File created successfully' });
};