const express = require('express');
const fs = require('fs').promises;
const morgan = require('morgan');
const asyncHandler = require('express-async-handler');

const app = express();

app.use(express.json());
app.use(morgan('tiny'));

const createFile = require('./controllers/createFile');
const getFiles = require('./controllers/getFiles');
const getFile = require('./controllers/getFile');
const deleteFile = require('./controllers/deleteFile');
const modifyFileContent = require('./controllers/modifyFileContent');

app.post('/api/files', asyncHandler(createFile));
app.get('/api/files', asyncHandler(getFiles));
app.get('/api/files/:filename', asyncHandler(getFile));
app.delete('/api/files/:filename', asyncHandler(deleteFile));
app.put('/api/files/:filename', asyncHandler(modifyFileContent));

app.use((error, req, res, next) => {
    error.message = error.status ? error.message : 'Server error';

    res.status(error.status || 500).json({ message: error.message })
});

app.listen(8080);