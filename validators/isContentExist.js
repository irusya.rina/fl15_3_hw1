const createError = require('http-errors')

module.exports = content => {
    if (!content) {
        throw createError(400, "Please specify 'content' parameter");
    }
}