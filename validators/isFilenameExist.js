const createError = require('http-errors')

module.exports = filename => {
    if (!filename) {
        throw createError(400, "Please specify 'filename' parameter");
    }
}