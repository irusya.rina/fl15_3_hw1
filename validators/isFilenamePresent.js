const fs = require('fs').promises;
const createError = require('http-errors');

module.exports = async (path, filename) => {
    try {
        await fs.access(path);
    } catch {
        throw createError(400, `No file with ${filename} filename found`);
    }
}