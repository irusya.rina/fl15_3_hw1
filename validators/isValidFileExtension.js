const createError = require('http-errors');

module.exports = extension => {
    const isExtensionValid = extension.match(/^(log|txt|json|yaml|xml|js)$/);

    if (!isExtensionValid) {
        throw createError(400, 'File extension is not valid');
    }
}